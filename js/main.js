var Brux = Brux || {};

Brux.Login = (function(win, doc) {
    'use strict';

    var cache = {};

    var validator = {
        isnull: function isNull(str) {
            return !!str;
        },
        passwordcheck: function passwordCheck(password) {
            if (!password)
                return false;
            /**
             * Password: must be at least 6 characters and contain at least one
			 * capital letter and at least one non-alphanumeric character.
			 **/
			var check = /^(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,}$/;
			return password.match(check);
        }
    }

    // cache DOM elements
    function setupCache() {
        cache.loginBtn = doc.getElementById('login-submit');
        cache.loginInputs = [].slice.call(doc.querySelectorAll('.login-form-input'));
        cache.loginPassworField = doc.getElementById('login-form-password'); // hmmm part of the set above and individually
        cache.hasClassList = (!!doc.documentElement.classList) ? true : false;
        cache.formErrors = doc.getElementById('form-errors');
        cache.docFrag = doc.createDocumentFragment();
        cache.passwordToggleBtn = doc.getElementById('login-form-password-toggle');
    };

    function addEvents() {
        cache.loginBtn.addEventListener('click', validateForm);
        cache.passwordToggleBtn.addEventListener('click', passwordToggle);
    }

    function validateForm(event) {
    	event.preventDefault();
        var errors = [];
        cache.formErrors.innerHTML = ''; // hmmm there oughta be a better way
        cache.loginInputs.map(function(input) {
            var inputdata = input.dataset,
                inputcheck = inputdata.valid,
                inputmessage = inputdata.message;

            if (!validator[inputcheck](input.value)) {
                errors.push(inputmessage);
                addNewClass(input, "input-error");
            } else {
                removeClass(input, "input-error");
            }
        });

        if(errors.length > 0) {

        	errors.forEach(function(err) {
        		var pg = doc.createElement("p");
        		addNewClass(pg, "form-error");
        		pg.textContent = err;
        		cache.docFrag.appendChild(pg);
        	});
        	cache.formErrors.appendChild(cache.docFrag);
        	addNewClass(cache.formErrors, "show-errors");
        } else {
        	removeClass(cache.formErrors, "show-errors");
        }

    }

    function passwordToggle () {
    	cache.loginPassworField.type = ( cache.loginPassworField.type === "password") ? "text" : "password";
    }

    /**
     * TODO: consolidate the classname logic
     **/
    function addNewClass(elem, name) {
        if (!elem)
            return;
        if (cache.hasClassList) {
            if (!elem.classList.contains(name))
                elem.classList.add(name);
        } else {
            if (!hasClass(elem, name)) {
            	console.log("add IE9");
                elem.className += ' ' + name;
            }
        }
    }


    function removeClass(elem, name) {
        if (!elem)
            return;
        if (cache.hasClassList) {
            if (elem.classList.contains(name))
                elem.classList.remove(name);
        } else {
            if (hasClass(elem, name)) {
            	console.log("remove IE9");
                var mclass = new RegExp('(\\s|^)' + name + '(\\s|$)');
                elem.className = elem.className.replace(mclass, ' ');
            }

        }
    }

    function hasClass(elem, name) {
        return !!elem.className.match(new RegExp('(\\s|^)' + name + '(\\s|$)'));
    }


    function init() {
        setupCache();
        addEvents();
    }

    return {
        init: init
    }

})(window, document);

// let's call here for simplicity
var login = Brux.Login;
login.init();
