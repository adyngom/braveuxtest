# Brave UX Login Screen #

In a normal setup this would be close to a release candidate with the few issues remaining. The distribution code can be found under the **dist/** directory or downloaded directly via the **dist.zip** file. To run the code you can clone
this repo:

```
git clone repo_name/braveuxtest.git && cd braveuxtest
```
and run a webserver locally. If you have node installed you add the http-server packgage globally 

```
npm install -g http-server
```

Then inside the **braveuxtest/** folder just run the command (note you could speccify a port number via the -p flag)

```
http-server .
```
This will by default open the page on **http://localhost:8080** if the port is available.

You can also download the **dist.zip** file, extract it and open the **index.html** file with your preferred browser.